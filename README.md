# Challenge 2

## Solving Problems with Arrays Methods & String Methods.

#### Steps
- Clone repo.

#### Deriverables
- Upload your github/gitlab repository url to Trello.

#### Final result

Our client has decided that it is important to use functions to solve the problem and he has
given us some tests to prove our functions. The statements/declarations in the
console.assert(…) describe the expected output from the function when provided with a given
input and should evaluate to true if you have written the function correctly.
